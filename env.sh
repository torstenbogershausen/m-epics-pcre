# File to download and unpack.
TARBALL=http://downloads.sourceforge.net/project/pcre/pcre/8.39/pcre-8.39.tar.bz2
# Path where source tarball was unpacked.
SRCPATH=pcre-8.39

PACKAGE_CONFIGURE_FLAGS="--with-pic"
